package problems.qbfpt.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LogFactory {
	
	private static  Logger logger; 
	
		
	public static Logger newInstance(String fileName) {
		logger = Logger.getLogger("Reporter");
		FileHandler fh;
		try {
			fh = new FileHandler("instances/logs/"+fileName);
			fh.setFormatter(new CustomFormatter());
			logger.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return logger;
	}
	

	private static class CustomFormatter extends Formatter {
		 
        @Override
        public String format(LogRecord record) {
            StringBuffer sb = new StringBuffer();
            sb.append(record.getMessage());
            sb.append("\n");
            return sb.toString();
        }
         
    }
	

}
