package problems.qbfpt.solvers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import problems.qbf.solvers.GRASP_QBF;
import problems.qbfpt.log.Log;
import solutions.Solution;

/**
 * @author blenoclaus, rodrigofreitas, felipepavan 
 * 
 * Problem MAX_QBFPT using Sampled Greedy Construction 
 *
 */
public class GRASP_QBFPT_SGC extends GRASP_QBFPT {

	private Integer p;

	public GRASP_QBFPT_SGC(Double alpha, Integer iterations, String filename, Integer p, Boolean bestImproving) throws IOException {
		super(alpha, iterations, filename, bestImproving);
		this.p = p;
	}
	
	@Override
		public Solution<Integer> constructiveHeuristic() {
		CL = makeCL();
		RCL = makeRCL();
		incumbentSol = createEmptySol();
		incumbentCost = Double.POSITIVE_INFINITY;

		/* Main loop, which repeats until the stopping criteria is reached. */
		while (!constructiveStopCriteria()) {

			double maxCost = Double.NEGATIVE_INFINITY, minCost = Double.POSITIVE_INFINITY;
			incumbentCost = ObjFunction.evaluate(incumbentSol);
			updateCL();

			/*
			 * Explore all candidate elements to enter the solution, saving the
			 * highest and lowest cost variation achieved by the candidates.
			 */
			for (Integer c : CL) {
				Double deltaCost = ObjFunction.evaluateInsertionCost(c, incumbentSol);
				if (deltaCost < minCost)
					minCost = deltaCost;
				if (deltaCost > maxCost)
					maxCost = deltaCost;
			}
			/* Choose a candidate randomly from the CL */
			Set<Integer> attempted = new HashSet<>();
			while (RCL.size() != Math.min(this.p, CL.size()) && attempted.size() != CL.size() ) {
				int rndIndex = rng.nextInt(CL.size());
				Double deltaCost = ObjFunction.evaluateInsertionCost(CL.get(rndIndex), incumbentSol);
				if (deltaCost <= minCost + alpha * (maxCost - minCost)) {
					RCL.add(CL.get(rndIndex));
				}
				attempted.add(CL.get(rndIndex));
			}
			attempted.clear();
			
			Double minValue = Double.MAX_VALUE;	
			Integer candidate = null;
			for (Integer value : RCL) {
				Double deltCost = ObjFunction.evaluateInsertionCost(value, incumbentSol);
				if (deltCost < minValue) {
					minValue = deltCost;
					candidate = value;
				}
			}
			if (candidate != null) {
				CL.remove(CL.indexOf(candidate));
				incumbentSol.add(candidate);
			}
			ObjFunction.evaluate(incumbentSol);
			RCL.clear();

		}

		return incumbentSol;
	}
	
	@Override
	public Solution<Integer> solve() {
		Log.info("*********************************************");
		Log.info("Heuristic: Sampled Greedy Construction");
		Log.info("Instance: "+fileName);
		Log.info("Alpha: "+alpha);
		Log.info("P: "+p);
		Log.info(bestImproving? "Best Improving" : "First Improving");
		return super.solve();
	}
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		GRASP_QBF grasp;
		try {
			grasp = new GRASP_QBFPT_SGC(0.5, 1000, "instances/qbf040", 10, Boolean.TRUE);
			Solution<Integer> bestSol = grasp.solve();
			System.out.println("maxVal = " + bestSol);
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("Time = "+(double)totalTime/(double)1000+" seg");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
