package problems.qbfpt.solvers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import problems.qbf.solvers.GRASP_QBF;
import problems.qbfpt.log.Log;

/**
 * 
 * @author blenoclaus, rodrigofreitas, felipepavan 
 * Classe que roda as variações das instâncias e das soluções 
 * implementadas
 *
 */
public class Reporter {
	
	private static final Logger logger = Logger.getLogger("Reporter"); 
	
	static String[] INSTANCES = {
				"instances/qbf020",
				"instances/qbf040",
				"instances/qbf060",
				"instances/qbf080",
				"instances/qbf100",
				"instances/qbf200",
				"instances/qbf400",
			}; 
	
	static Double[] ALPHA = { 
			0.2, 
			1.0		
		}; 
	
	public static void main(String[] args) throws IOException {
		FileHandler fh = new FileHandler("instances/Report.log"); 
		logger.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);
		
		List<GRASP_QBF> solvers = Arrays.asList(
				/*new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[0], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[1], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[2], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[3], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[4], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[5], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[6], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[0], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[1], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[2], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[3], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[4], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[5], Boolean.TRUE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[6], Boolean.TRUE),
				
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[0], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[1], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[2], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[3], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[4], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[5], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[0], 1000, INSTANCES[6], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[0], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[1], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[2], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[3], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[4], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[5], Boolean.FALSE),
				new GRASP_QBFPT(ALPHA[1], 1000, INSTANCES[6], Boolean.FALSE),*/
								
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[0], 5, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[1], 10,  Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[2], 30, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[3], 50, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[4], 60, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[5], 70, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[6], 100, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[0], 5, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[1], 10, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[2], 30, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[3], 50, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[4], 60, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[5], 70, Boolean.TRUE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[6], 100, Boolean.TRUE),
				
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[0], 5, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[1], 10,  Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[2], 30, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[3], 50, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[4], 60, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[5], 70, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[0], 1000, INSTANCES[6], 100, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[0], 5, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[1], 10, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[2], 30, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[3], 50, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[4], 60, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[5], 70, Boolean.FALSE),
				new GRASP_QBFPT_SGC(ALPHA[1], 1000, INSTANCES[6], 100, Boolean.FALSE), 
				
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[0], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[1], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[2], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[3], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[4], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[5], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[6], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[0], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[1], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[2], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[3], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[4], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[5], Boolean.TRUE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[6], Boolean.TRUE),
				
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[0], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[1], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[2], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[3], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[4], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[5], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[0], 1000, INSTANCES[6], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[0], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[1], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[2], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[3], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[4], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[5], Boolean.FALSE),
				new GRASP_QBFPT_CP(ALPHA[1], 1000, INSTANCES[6], Boolean.FALSE)
				
				);
			
			for (GRASP_QBF solver : solvers) {
				try {					
					solver.solve();
				}catch (Throwable e) {
					Log.info("error");
				}			
			}
		
	}

}
